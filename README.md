# ~/ABRÜPT/JOSÉPHINE LANESEM/CONTRE LA MORT/*

La [page de ce livre](https://abrupt.cc/josephine-lanesem/contre-la-mort/) sur le réseau.

## Sur le livre

Suivre les pas d'Orphée, devenir son ombre, son souffle, emprunter sa voix. Aller contre la mort, à sa rencontre, tout contre. Des hautes forêts de Thrace aux marais soufreux des enfers, de la source qui enfanta Eurydice à la rive qui donna naissance à l'orphisme.

Orphée, un nom qui définit la poésie, et dont la destinée se résume au déchirement : déchirer la langue, fouiller sa déchirure, à la manière de sorciers jugeant du foie de leurs victimes, déchirer la chair, pour chercher le désir absent ou réifier ses rêves.

Initiation aux arcanes de l'intériorité, à notre propre obscurité. Formule de l'éternité qui luit sur les lamelles enfouies dans les tombes. Paroles du fond des âges qui résonnent jusqu'ici et maintenant, nous obligeant à nous retourner, à faire face à la fatalité.

Mystères : une série autour du mythe, dont le sens ne s’épuise pas, non parce qu’il serait illusoire et au final absent, mais parce qu’il est mobile et multiple. La réécriture est une résurrection.

## Sur l'autrice

Née à Paris, elle y a grandi et étudié la philosophie et l'histoire de l'art. Elle a vécu à Cagliari, Lisbonne, Berlin et Barcelone, avant de s'établir à Trieste. Un jour, elle parle italien, l'autre français, et la langue des rêves, elle l'écrit.

[Nervures et Entailles](https://josephinelanesem.com/), le site de Joséphine Lanesem.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
