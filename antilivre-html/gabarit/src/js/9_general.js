// Scripts

// Interface
const menu = document.querySelector('.btn--menu');
const title = document.querySelector('.title');

const interface = document.querySelector('.interface');

menu.addEventListener('click', (e) => {
  e.preventDefault();
  title.classList.toggle('show');
  menu.classList.toggle('btn--anim');
});

const btnOrphee = document.querySelector('.btn--orphee');
const chapitreOrphee = document.querySelector('.chapitre--orphee');
btnOrphee.addEventListener('click', (e) => {
  e.preventDefault();
  chapitreOrphee.classList.add('chapitre--show');
  interface.classList.toggle('hide');
  btnClose.classList.toggle('hide');
});

const btnMenade = document.querySelector('.btn--menade');
const chapitreMenade = document.querySelector('.chapitre--menade');
btnMenade.addEventListener('click', (e) => {
  e.preventDefault();
  chapitreMenade.classList.add('chapitre--show');
  interface.classList.toggle('hide');
  btnClose.classList.toggle('hide');
});

const btnInitie = document.querySelector('.btn--initie');
const chapitreInitie = document.querySelector('.chapitre--initie');
btnInitie.addEventListener('click', (e) => {
  e.preventDefault();
  chapitreInitie.classList.add('chapitre--show');
  interface.classList.toggle('hide');
  btnClose.classList.toggle('hide');
});

const btnEurydice = document.querySelector('.btn--eurydice');
const chapitreEurydice = document.querySelector('.chapitre--eurydice');
btnEurydice.addEventListener('click', (e) => {
  e.preventDefault();
  chapitreEurydice.classList.add('chapitre--show');
  interface.classList.toggle('hide');
  btnClose.classList.toggle('hide');
});

const btnClose = document.querySelector('.btn--close');
btnClose.addEventListener('click', (e) => {
  e.preventDefault();
  const chapitre = document.querySelectorAll('.chapitre');
  chapitre.forEach((c) => {
    c.classList.remove('chapitre--show');
  });
  interface.classList.toggle('hide');
  btnClose.classList.toggle('hide');
});

const cursor = document.querySelector('.cursor--main');
const cursorBg = document.querySelector('.cursor--bg');
const links = document.querySelectorAll('h1, .title__link, .btn');

let cursorX = -100;
let cursorY = -100;
let cursorBgX = -100;
let cursorBgY = -100;
let pageLoad = true;

const cursorMove = () => {
  document.addEventListener('mousemove', (e) => {
    cursorX = e.clientX;
    cursorY = e.clientY;
    if (pageLoad) {
      cursorBgX = e.clientX;
      cursorBgY = e.clientY;
      pageLoad = !pageLoad;
    }
  });

  // Lerp (linear interpolation)
  const lerp = (start, finish, speed) => {
    return (1 - speed) * start + speed * finish;
  };

  const rendering = () => {
    cursorBgX = lerp(cursorBgX, cursorX, 0.1);
    cursorBgY = lerp(cursorBgY, cursorY, 0.1);
    cursor.style.left = `${cursorX}px`;
    cursor.style.top = `${cursorY}px`;
    cursorBg.style.left = `${cursorBgX}px`;
    cursorBg.style.top = `${cursorBgY}px`;
    requestAnimationFrame(rendering);
  };
  requestAnimationFrame(rendering);
}

cursorMove();

// Interaction with the cursor
 links.forEach((e) => {
   e.addEventListener('mouseenter', () => {
     cursor.classList.add('cursor--interact');
     cursorBg.classList.add('cursor--interact-bg');
   });
   e.addEventListener('mouseleave', () => {
     cursor.classList.remove('cursor--interact');
     cursorBg.classList.remove('cursor--interact-bg');
   });
 });

 document.addEventListener('click', () => {
   cursor.classList.add('cursor--click');

   setTimeout(() => {
     cursor.classList.remove('cursor--click');
   }, 250);
 });

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

const textes = document.querySelectorAll('.chapitre p');
for (let i = 0, len = textes.length; i < len; i++) {
  textes[i].innerHTML = textes[i].innerHTML.replace(/(^|<\/?[^>]+>|\s+)([^\s<]+)/g, '$1<span class="mot">$2</span>');
}

const mots = document.querySelectorAll('.mot');
for (var i = 0, len = mots.length; i < len; i++) {
  mots[i].addEventListener('mouseenter', (e) => {
    if (e.target.classList.contains('mot--anim')) {
     	e.target.classList.remove('mot--anim');
      void e.target.offsetWidth;
     	e.target.classList.add('mot--anim');
     }
    e.target.classList.add('mot--anim');
  });
  mots[i].addEventListener('mouseleave', (e) => {
   setTimeout(() => {
     e.target.classList.remove('mot--anim');
   }, 19000);
  });
}
