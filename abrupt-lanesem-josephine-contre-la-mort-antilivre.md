---
author: "Joséphine Lanesem"
title: "Contre la mort"
subtitle: "Mystères"
publisher: "Abrüpt"
date: 2020
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0123-1'
rights: "© 2020 Abrüpt, CC BY-NC-SA"
adresse: "https://abrupt.cc/josephine-lanesem/contre-la-mort"
---

# 🜍

Lève-toi, fumée amère. J’ai descendu d’innombrables marches de brume et de brouillard. Elles se retiraient sous mes pas en gémissant. Je suis arrivé aux confins de la terre, à son centre si le néant a un centre. La bête dormait. Elle ne se réveille qu’à l’approche de la peur, et je suis sans peur. Le pire est déjà arrivé. Tu m’as quitté. Mais je te rattraperai. Aussi loin que tu ailles et même au-delà des enfers, dans le chaos d’un ciel sans étoiles.

Lève-toi, ombre d’âme. Les Furies furètent dans les coins, elles reniflent mes traces. Mais les serpents ne sifflent plus sur leurs têtes et les larmes de sang ont séché sur leurs joues. Les Moires lèvent leur visage sévère des aiguilles qui percent et rapiècent nos destins. Leurs yeux caves captent une aurore appauvrie. Ces puissances anciennes et sombres se rencontrent dans les entrailles de la terre. L’oreille dure, elles s’approchent pour m’écouter et se répètent mes paroles, en articulant lentement. Les vénérables nous laissent une chance. Elles ont pitié de toi, une femme comme elle. Morte d’être femme.

Lève-toi, poussière sainte. C’est toi, je le sais. Je te reconnais. Tu n’as pas encore refroidi. Ton bûcher n’a pas fini de se disperser dans le ciel. Il s’éloigne sur un fleuve qui pleure à déborder ses rives. Tes os ont craqué comme du bois vert à la première flamme. Tu es bien fille de chêne. Je ne me suis pas attardé à te regarder disparaître, j’ai cherché au plus vite l’entrée des ténèbres, courant après ton dernier souffle.

Lève-toi, petite cendre. Vite. Avant qu’ils ne nous voient. Perséphone brosse ses cheveux vermeils, ils crépitent sur le blanc de ses bras. Hadès tousse au-dessus d’un crâne gorgé de sang, se frottant le nez à la pestilence d’un vivant. Ils ne m’ont pas vu passer sous les fenêtres de leur palais d’os, d’ivoire et de corne. J’ai les pieds du poète, légers au bord des précipices.

Lève-toi, Eurydice. Il est temps.

Mais tu ne sais pas comment. Tu n’as plus de pieds, plus de colonne pour initier ta verticalité. Commence par le souffle. Dresse-toi comme la flamme qui t’a consumée. Qui te retient ? Quel charme ? Quel monstre ? Je suis prêt à l’affronter. Avec ma voix, mon seul don. J’ai si longtemps chanté la vie, ce sera mon arme contre la mort. Tu ne veux pas ? Ce séjour te plaît ? Il est calme. Ici, personne ne te fera de mal. Tout le mal t’a déjà été fait. L'homme, charmé par ta beauté, te poursuivait dans la forêt et un serpent, surpris par ta course, creva ta cheville de son venin.

Aristée aux mille abeilles, qui n’avait jamais assez de miel, et voulut même dérober tes rayons. Les dieux l’ont puni, tu sais ? Toutes ses abeilles sont mortes. Mais ajouter la mort à la mort ne redonne la vie à personne. Et les dieux sont moins constants que mon amour. L’assassin leur a sacrifié douze taureaux. Satisfaits, ils ont libéré de nouveaux essaims de leurs chairs lacérées.

Au moins a-t-il eu assez honte pour s’exiler. Quittant nos montagnes, il est parti pour une île lointaine. L’air bourdonnait d’abeilles autour de lui. Les arbres s’écartaient avec hostilité. Les sources se retiraient à ses lèvres. Il est allé sans ombre ni eau jusqu’à cette terre solitaire, réputée pour ses fleurs têtues et entêtantes. Puissent ses abeilles lui infliger sans trêve des piqûres aussi cruelles que la morsure qui t’a privé du jour.

Je ne pardonne pas à qui force l’amour.

Mais ce serpent, l’as-tu cherché dans l’herbe ? L’as-tu supplié de te mordre pour échapper aux assauts d’Aristée ? Ou taraudée par l'inavouable envie de mourir ? De te réduire à ce monticule de cendres, toi, fille éclose de la source et du chêne, dryade qui ne connaissait que la verdeur de vivre ?

Je te connais si mal. Je t’ai trop aimée pour te connaître. Dans le jardin de l’Extrême-Occident, où le soleil, appointé au couchant, baigne de rose une contrée allongée dans la douceur de presque mais jamais vraiment, j’ai fréquenté les nymphes les plus accomplies qu’ait enfantées l’Olympe. Mais je t’ai choisie, toi, une anonyme des bois communs à ma région natale, sauvage à peine humaine, encore humide de la boue et des brindilles dont elle a émergé. Proche, familière, et pourtant plus étrangère que ces beautés du bout du monde. Car la beauté est commune, tout le monde s’accorde pour l’admirer. Tu étais différente. Aristée ne s’y est pas trompé, il sait détecter la rareté d’un nectar.

Je t’ai aimée pour cette graine de feu dans ta verdeur, pour l’incendie que tu portais de toute forêt, pour ton ardeur à laquelle aucun monde ne suffirait. Aucun monde. Même le néant. Je le comprends maintenant. Car toi, tu ne m’as pas choisi. Je n’étais qu’un Aristée, moins la violence, moins l'empressement, rêveur qui se serait contenté de chanter ta présence au lieu d’en profiter. M’as-tu accepté parce que j’étais sans danger ? Ou pour offrir à ta famille sylvestre un gendre servile qui savait attirer le soleil, éloigner la foudre et émouvoir la pierre ?

Comment savoir si tu m’aimais ? Tu me souriais. Mais à qui ne souriais-tu pas ?

Tu souffrais de ton origine trouble, je le sais, d’un père trop vieux et d’une mère trop jeune. Lui, à la tendresse austère, te gardait jalousement dans l’ombre de ses pairs. Elle glissait entre tes doigts. Plus enfant que toi, elle prenait sans savoir donner. Tu as hérité des deux, à la fois frivole et grave, oublieuse et pensive. Un t’aimait trop et l’autre pas assez. On aime toujours mal, Eurydice. Tu l’aurais su, si tu t’y étais risquée.

On me donne pour parents une muse et un roi. N’en crois rien. C’est de la modestie que naissent les merveilles. Ma poésie vient du linge que ma mère étendait au soleil, des brebis que mon père réunissait au soir. Je suis comme toi un enfant des montagnes, mal dégrossi parmi les dieux qui m’acceptent à leur table. Je ne maîtrise qu’un art : attirer la beauté du monde qui m’entoure, offrir ma voix à son passage. Berger d’épiphanies, lingère en ravissements, telle est ma profession.

Je ne me suis pas aventuré ici pour la gloire, la prouesse. Ou si, un peu, au début. Mais depuis que je descends parmi ces brumes où s’effilochent un par un mes souvenirs et mes sens, je ne marche plus que pour la douleur qui bat à rompre planète et poitrine, pour cet axe de souffrance qui traverse chaque être et l’enracine au ciel, ce cri muet au plein des pierres où crissent les étoiles.

L’entends-tu ?

Je renoncerais volontiers à ma voix, à mon nom, à mes honnêtes montagnes pour me cacher avec toi dans quelque antre, vivre sans histoire, et un jour être vieux. Découvrir à tes côtés la monotonie d’aimer, le temps qui nous travaille d'impatience, nos visages qui se feront racine. Je ne veux pas me survivre dans la mémoire des hommes, je veux être vivant ici et maintenant, et être vivant, c’est être avec toi.

Je t’ai reconnue à ta graine de feu. Elle brûle encore dans ces ténèbres et te distingue d’entre toutes les cendres. Elle ne te vient ni du chêne ni de la source. De quelle divinité ? De quelle origine sacrée ? L’as-tu inventée seule dans l’alchimie de ta vie nouvelle ?

Comme tu as bien brûlé sur ton bûcher. Du bon bois que tu es. D’y penser, j’ai envie de hurler. Mais je n’ai pas le droit. Orphée doit moduler le deuil, articuler la peine. Il lui est interdit de pleurer sans mot ni musique. Orphée doit chanter jusqu’à l’assassinat de toute joie.

Viens, et qu’en toi soient rachetées toutes les morts, consolées toutes les pertes. Donne-nous l’espérance. Et si la vie est trop basse pour toi, envole-toi, échappe-toi de ces contrées. Promets-nous un autre au-delà que la poussière. Mais ne renais pas sous quelque forme que j’ignore. Ne me condamne pas à te chercher dans le réseau, l’orbe, le mufle, le bec et l’épi, à sonder l’œil blond des bêtes ou celui obtus des hommes, ne te dissimule pas dans quelque enfant que je n’aurai pas le temps de connaître femme.

Reviens-moi en Eurydice, fille des feuilles, sœur de la nymphe et du cerf. Tous t’aimaient, ton père comme le surgeon d’une espèce nouvelle, aussi coriace que le soleil, tes frères comme la fin de la chasse, la senteur tourbeuse des clairières après l’averse, la fatigue bénéfique d’avoir couru longtemps sur des sabots minces et francs, tes sœurs comme une part d’elles-mêmes, parcelle de l’âme unique qui se tisse sans césure entre toutes les nymphes, qu’elles émanent des grottes, des bois, des rivières ou des mers, et même des marais fangeux et soufreux du Tartare, de la neige éternelle au sommet de l’Olympe ou de la pluie errante et des vents voyageurs, une seule âme impalpable de l’un à l’autre corps où ta mort ouvre le trou par où tout se défait et toutes meurent.

Si ce n’est pour mon amour, trop récent pour durer, trop neuf pour t’y fier, reviens pour le leur, qui t’a toujours accompagnée. Tiens la promesse de ta naissance. Ils y ont cru.

Tu résistes. Je le sens. Je ne peux pas croire que tu aimes cet état. Tu t’y plais parce que tu as oublié qui tu étais, et tu étais tant de choses. Déjà, la sourcière. Les hommes venaient à toi pour te demander où creuser le puits et élever la fontaine. Tu prenais un rameau, l’abaissais vers le sol et suivais ses soubresauts. Clarté dissimulée dans la matière que tu révélais soudain. Eau neuve et vigoureuse, dissipant l'amertume. On a fondé des villages autour de tes miracles.

Tu m’as confié au bord d’une de ces veines à peine jaillies de terre : "La mort, je n’y crois pas. Je n’arrive pas à l’imaginer." Et maintenant, tu ne crois plus à la vie. Décidément, tu manques d’imagination. Comment te la décrire ? Le frisson d’être à la fois fort et faible, l’impatience et la langueur mêlée de croître et désirer. Tiens, je m’entaille largement la main, du pouce au poignet. Que ton feu s’abreuve à mon sang, que ta cendre se repaisse de ma chair. Prends mon corps. Partageons-le. Si tu ne me rejoins pas, il est de trop entre toi et moi.

Reviens même pour ne plus m’aimer. Je vivrai pour te chanter. Tu m’étais promise, je te libère de ta promesse. Je paie ici la dette des hommes envers les femmes. Elle dure et je ne l’ai pas accrue. Mais je suis le dernier de cette longue lignée.

Triste race des nymphes, pourvue de tant de grâces. Il aurait mieux valu être nées obscures. Combien d’entre vous ont disparu sous le crime d’un homme ? Réduites en poussière, changées en cavale, en louve ou en étoile, privées d’un corps que ne rachètera aucune gloire, fût-elle inscrite au ciel. On ne m’a raconté que vos tribulations. Personne n’a gardé la mémoire de vos joies. Mais qui les connaissait ? Quel homme s’est demandé quelle était votre vie avant de l’écraser ?

Ta vie était secrète, retirée. "Vivons heureux, vivons cachés", voilà ta devise, et ton malheur a été d’être trouvée. Tu soufflais la fuite à tes frères, tes tours égaraient les chasseurs, même l'archère pâle et leste des frontières ne trouvait plus la piste. Frustrée de ses forfaits, elle respectait ta fierté, ta ruse et t’envoyait ses apprenties les plus aguerries, afin de les former.

Tu haïssais le sang qui gicle autant que tu aimais l’eau qui fuse. Plus végétale qu’animale, lumière mariant la fibre à l’onde. Vivante qui aimait le vivant, toute germination, gorgée de suc jusqu’à la crevaison. Et tu es morte, disons-le, si sottement. La mort est une sottise. L’intelligence se manifeste dans l’infini réseau de la vie.

Portant secours aux délaissées, aux silenciées, tu t’asseyais auprès d’une inconnue réduite à l’atonie d’aimer sans être aimée, et tu ne te détournais pas de l'ancienne ennemie, qui autrefois traquait tes frères dans leurs derniers repaires. Violée par un dieu tout-puissant, insultée par ses comparses soucieuses de pureté, changée en ourse par une épouse jalouse, seule et si défigurée que son fils hésitait à l’achever d’une flèche. Tu l’écoutas ravager la forêt, tu grimpas dans le frêne lui rapporter du miel, tu serras dans tes bras cette masse lourde, rêche, crasseuse, qui avait été la plus splendide, la plus intrépide des nymphes attachées à la suite de l'arc et du croissant. Peut-être pressentais-tu l’approche d’un sort semblable.

C’est moi qui t’ai trouvée. Étendue dans l’herbe. Face contre terre. Aristée enfui n’avait pas eu la décence de te relever.

Je t’ai tenue glacée et j’ai compris que tu avais été chaude. Glaise tiède que n’égalera aucun marbre de la mémoire. Je t’ai tenue flasque et disloquée et j’ai compris que tu avais été pleine et entière. Muscles ligneux, joues fruitées, yeux de résine et langue de source. Je t’ai tenue lente, entravée, ensevelie déjà dans le pays des songes, et j’ai compris que tu avais été rapide, réelle, déchirant l’espace de ta présence.

À mon cri, ton père s’est pétrifié, plus noir que frappé par la foudre. Je ne sais pas s’il reverdira. Les cerfs ont brisé leurs bois contre les troncs et leur sang se mêlait à l’ambre. Les dryades ont disparu dans les feuillages et leurs larmes fleurissaient au pied des arbres. La source s’est tarie. Tous avaient bruissé, chanté, célébré notre passion naissante. Ils nous cernaient à présent de leur silence.

De toi, il ne reste rien. Aucun souvenir solide. Les nymphes n’ont pas de possession. Vous n’êtes que passage. Mourir inflige une condition trop grave à votre légèreté. D’ailleurs, sans homme à vos trousses, vous mourez rarement.

Je n’aurais jamais dû être celui qui reste, la soustraction de notre union, la somme de nos souvenirs. Celui qui se déchire, quand l’âme se retire, mais que le corps reste, par une opération inverse et sœur de celui qui meurt.

La forêt t’oubliera. Renaître est dans sa nature. Remplacer est sa loi. Les bois repousseront au front des cerfs. Les nymphes retisseront la vie passante. Elles tresseront l’air à la pierre, le rayon à la rive, le mâle à la femelle, comme elles l’ont toujours fait, avec et sans toi. Leur amour se conjugue au passé, le mien à l’avenir. Seul je t’ai aimée pour ton unicité, pour cette lueur que tu n’as héritée de personne, quintessence décantée par ta chaleur secrète. Je ne saurais te remplacer.

Tu m’offrais la récompense de longues souffrances. Je n’ai pas eu le temps de te raconter mon périple, notre désastreuse réussite :\
la vaste nef issue du vertige des forêts,\
nos rames martelant la mer noire,\
le courant bousculant notre coque,\
les sirènes voraces amassant les cadavres\
et les baleines plus attirantes que les pôles,\
les camarades dont le visage s’altérait,\
le mien que je ne voyais plus depuis des mois,\
parmi nous le fils de Borée dont les ongles gelés égratignaient à la nuit ses étoiles,\
les siamois qu’ont dit jumeaux à la jeunesse décuplée,\
insurpassables aux armes,\
un colosse aussi grand que nos mâts,\
aussi généreux que nos coffres,\
dont le poids basculait la quille,\
mais sa brutalité était une forme de bonté,\
ses excès devenaient des exploits,\
et Tiphys, le premier pilote comme je suis le premier poète,\
bel équipage,\
tant de héros,\
dont il ne reste pour la plupart que des sépultures anonymes,\
sur des îles vierges du pas de l’homme et de la main du dieu,\
enfin dans la lointaine Colchide,\
le prix dérisoire de tant de maux,\
la toison ruisselante d’or du bélier sacré,\
un dragon la gardait, son sang éclaboussa nos bras,\
ses dents éparpillées semèrent d’innombrables guerriers,\
la terre les ravala à un geste de Médée,\
souveraine juste et vraie,\
révoltée par les pièges de son père,\
mais son intégrité nuit aux intérêts des grands,\
calomniée de sa terre natale à celle de son exil,\
discréditée face au peuple qu’elle cherche à libérer,\
elle succombera sûrement à leur haine,\
à la honte de susciter la haine.\
J’en oublie le retour,\
plus aléatoire que l’allée, selon la loi des mers,\
Circé dont les maléfices donnent au corps l’apparence de l’âme,\
si rarement belle à voir,\
et beaucoup de nos hommes se prélassent encore en cochons dans ses auges,\
le long des plages de la Crète,\
en sentinelle,\
le géant de fer,\
dont la mécanique s’enraye par une clef à la cheville,\
qu’il suffit de tourner,\
encore grâce à Médée,\
femme trop avisée de nos faiblesses,\
puis, à l’approche du pays,\
un tourbillon où le feu des enfers se déverse et soulève les abysses,\
manquant d’engloutir notre nef plus profond que l’Hadès.

Notre légende me précède. Elle vante mon chant qui enchaîna les sirènes à leur charnier, attacha les poissons à notre sillage, écarta les rochers de notre proue douée de prophétie. Elle dit vrai, mais je ne me félicite que d’avoir donné force et courage à mes compagnons de voyage. Nous n’étions qu’un grand corps auquel j’impulsais la cadence. Pas comme avec tes sœurs. Les hommes ont d’autres communions. Nous n’étions qu’un grand corps d’effort et de vouloir.

Cependant, notre équipage comptait une femme. Atalante, aussi brave et bien bâtie qu’un homme, s’épuisait à prouver la supériorité de votre sexe sur le nôtre.

Si tu revenais, je te raconterais. J’ai tant d’histoires. Tu vivrais plus d’une vie.

Te voilà. La curiosité te ressuscite. Tu n’es encore qu’une flamme. C’est déjà mieux que cendres. Suis ma voix. Reprenons ces marches qui n’autorisent que la descente et précipitent la chute. Soyons les premiers à en transgresser l’ordre et le sens.

Non, ne te disperse pas. D’où vient ce courant d’air ? Qui souffle contre toi ? Tiens bon. Rappelle-toi la vie.

Le printemps était ta saison et le matin ton heure. Tu aimais la beauté avec simplicité. La clairière quand elle a ses fleurs, le ciel quand il a son soleil. L’arbre dans sa puissance et l’eau dans sa pureté.

Où dorment les dryades ? Où vous surprend l’aurore ? À la cime ou entre les racines ? Tu m’échappais dès la nuit tombée. Aurais-tu supporté l’étroitesse d’une couche sur le dallage nu, entre des murs immobiles que l’ombre rapproche ? Rien n’aurait respiré autour de toi, à part moi. J’aurais été le seul vivant à te rappeler la vie. Comme ici. Mes bras t’auraient enserrée aussi délicatement que tes chrysalides de mousse, ma voix te serait devenue aussi familière que le frémissement des feuillages, et le plaisir, peut-être, t’aurait constellée d’autres regrets.

Je ne t’ai vue dormir qu’une fois. Assoupie par mégarde, épuisée par nos jeux. L’après-midi finissait et tu verdissais. Reflets à tes cheveux, lentilles sur ta nuque, tes épaules. Dès ton réveil, cette couleur régressait dans les méandres de tes veines et n’apparaissait qu’en faisceau à la transparence de tes poignets. Les humains dorment pour se reposer, les nymphes pour rêver. N’en tire pas vanité. Tu ne subis pas l’étau étroit de la réalité. Ta forêt t’obéit au doigt et à l’œil, et presque rien ne la distingue du rêve. Tu ne sais rien de la faim, de la fatigue. La mort a été ta première privation. Les humains ne sont pas durs par choix, mais par condition. Pardonne à un cœur où le manque a martelé sa marque.

Ta silhouette se dessine. Ta flamme s’éclaircit en incarnat. J’entends ton pas, le froissement de ta robe entre tes cuisses, ton souffle qu’accélère la marche, je m’enivre à ton odeur de sève, de sel, je sais, par ces sens qui n’ont pas de nom, que tu souris déjà, même sans lèvres.

Seule senteur, seule sonorité dans l’interminable poussière. Elles couvrent aisément les miennes.

Vie de nymphe bien plus intense que celle d’un homme.

Tu t’arrêtes, tu hésites, confuse d’à la fois sentir, penser, espérer, regretter. Oui, la vie est floue. La poésie sert justement à faire le point. Rassure-toi. Vivre n’a aucun sens ici, parmi les ombres. Moi aussi, je tremble. Mais à la surface, dans ta forêt native, environnée de sa verdeur douce-amère, tu reprendras rythme et relief, tu retrouveras ton harmonie.

Rentre dans la durée, au risque de te disperser dans l’éphémère. Sens le temps te disjoindre en une succession. Avance-toi dans l’imprévu, l’incertain, l’inachevé. Cherche aveuglément ta croissance, c’est la seule manière de poindre.

Résiste, en digne fille de l’écorce et de la corrosion, contre l’inertie de mourir et la pression du même et du perpétuel.

La forêt nous rejoint, elle t’encourage, la voici qui étend son emprise à l’empire des morts, disséminant graines et spores dans ce lieu où ne s’aventure aucun vent. Les conifères, dont les racines firent les premières céder la pierre, les fougères, qui festonnèrent une terre que ne creusaient pas encore les enfers, maintiennent ferme sous nos pas le sol gémissant de la poussière.

Serre ce nœud de sang qu’on appelle le cœur, qui relie chaque point de ton corps et ton corps à la terre. Referme ta chair sur l’énigme de ton être. Refuse à la lumière ton intériorité. Recrée ce secret qui nous fait. Tu es morte d’avoir été éclairée. Le venin, lampe jaunâtre, a trahi les buissons de ton sang. Le feu les a rendus à leur obscurité.

Oublie la clarté glauque de l’agonie et j’oublierai ton profil livide dans la forêt en deuil et l’herbe gourmande aux franges de tes jambes.

Je sais que tu t’es débattue, que tu m’as appelé. Maintenant, je suis là, plus rien ne nous séparera. Si nous ne trouvons pas la porte du jour, nous resterons entrelacés dans l’ombre, fiancés des ténèbres, protecteurs des serments qu’oblige la distance, des promesses que commande la jeunesse.

Ta vie s’épanouit. Perséphone se doute de l’intruse. Appelant la bête :

«Qui est entré dans les enfers ?\
--- Personne.\
--- Il y a quelqu’un.\
--- Je n’ai senti aucune peur.\
--- Qui serait entré ici sans avoir peur ?»

Hadès bondit, impatient de combattre un héros, ne sachant affronter qu’un poète. Les Moires tremblent. La plus clémente, chargée d’initier la vie, non de la poursuivre, ni de l’achever, nous indique une issue. Ses cadettes ne nous retiennent pas davantage. Les mains derrière le dos, elles lâchent notre destin, le laissent libre de liens. "Vite ! Sauvez-vous ! Par là !" Chuchote la première, et les secondes acquiescent.

Ne faiblis pas. Rappelle-toi respirer. Rappelle-toi aimer.

Tu trébuches de retrouver ton poids. Pèse donc sur le sol pour mieux t’élancer vers le ciel. Ton poil se hérisse à l’horreur d’être morte. Cela ne durera pas. Nous y sommes presque. Je vois ton œil incarner la lumière, tes joues rougir de notre course, tes lèvres luire d'espérance, tu as retrouvé ton visage, et tu ne sais si rire ou pleurer. Bientôt viendra la voix.

Les Furies font diversion, elles se chamaillent dans le claquement de leurs ailes cireuses et de leurs brefs fouets. Perséphone n’est pas dupe. Elle ordonne le silence. Hadès s’arme de son casque, son bouclier, sa lance. La gardienne des nuits sans lune descend du carrefour entre ciel, terre et mer, où elle effritait les os du sacrifice, en extrayait la moelle et s’en repassait pour trouver la force de veiller. Elle les fixe, échevelée, de ses yeux cernés d’insomniaque, puis annonce d’une voix sans timbre : "Orphée s’enfuit avec Eurydice", avant de bâiller si ouvertement que de ses lèvres gercées s’échappent trois lunes pleines, qu’elle offre en galettes aux trois têtes de la bête. Hadès grogne : "Depuis que l'enfant du tonnerre est revenu des enfers, je suis déshonoré. Maintenant, même les poètes viennent piller mon royaume." Perséphone retient sa main. "Laisse-les. Ne m’as-tu pas ravie au jour ? Laisse Orphée ravir Eurydice à la nuit. En l’honneur de notre amour."

Dans la reine des ténèbres, aux yeux d’inexorable hiver, aux pieds d’été fringant s’anime l’innocente Coré, la jeune fille de Sicile qui cueillait des narcisses quand l'enfer l’enleva. Elle composa de ses fleurs trois couronnes qui luisent encore au front des Moires. Son mari, songeur, oublie son honneur. Il cueille sans y penser le pavot et l’aconit de leur jardin empoisonné.

Eurydice, tu es libre.

La vie approche. Je vois déjà le ciel de nos montagnes, les cimes de nos arbres, la grotte où je me réfugiais tandis que tu dansais sous l’averse. Par là je suis descendu jusqu’à toi. Ne crains rien, aucun mal n’est pire que de ne rien souffrir. Viens, prends ma main.

Que t’arrive-t-il ? Tu pâlis. Ne fallait-il pas te toucher ? N’avais-tu pas fini de ressusciter ? Réponds-moi. Non, ne recule pas. Ne retourne pas à l’ombre. Je ne te vois plus. Où es-tu ? Même ta flamme a disparu. La pierre se referme. Je m’y fracasse le front. Ne s’ouvrent que les portes de ma douleur.

Tu t’es dissipée dès que je t’ai touchée, comme si tu n’étais qu’un rêve. Le délire de ma fièvre, le mirage de mon désert. Suis-je descendu jusqu’à toi ? Ou n’ai-je que hurlé, halluciné, dans cette grotte ?

Non, j’entends le grondement des marches qui s’effondrent et enterrent ta chute, et ma main brûle encore d’avoir touché la tienne.

# ☿

Ne me condamnez pas. Je ne suis pas seule à porter la faute. On n’est plus soi parmi les autres.

Depuis son retour, Orphée ne prononçait pas un mot, et son silence devint vite une légende plus inoubliable que sa parole. On avait cru sa passion passagère, la consolation du marin éprouvé, l’engouement d’un artiste naïf, mais Orphée mettait tout son cœur en toute chose.

Il vivait dans la grotte où il avait aperçu une dernière fois sa fiancée. Son front portait les marques de la pierre. Ses cheveux avaient blanchi. Sa bouche était une cicatrice, la parole une plaie qui ne guérissait pas, à laquelle il ne touchait plus. Un jour, il creusa la terre à mains nues et s’enterra vivant. Un autre, il erra sur les cimes, dans la neige et revint les pieds brûlés. Il hésitait au bord des précipices, mais on ne se déracine pas si facilement, surtout quand on a chanté sa vie durant la beauté d’être au monde.

Qui n’a pas aimé Eurydice ? Surtout nous, les femmes. Ses sources abreuvent nos villages et donnent à nos enfants santé et bonne humeur. Quand elle nous croisait, elle nous aidait à porter le seau ou essorer le linge, y laissant l’empreinte parfumée de ses mains qui descendent des fleurs. Elle ne tolérait pas les chasseurs. En son temps, la forêt était sûre. On ne craignait pas le prédateur, qu’il soit homme ou animal. Mais elle est morte et il faut vivre. Elle n’était pas la seule nymphe de la forêt. Orphée, lui, était notre seul poète et nous sommes sourds de ne plus l’entendre.

Le printemps revint pour la troisième fois. Perséphone délaça ses sandales à la sortie des enfers et courut pieds nus rejoindre le plein air. À chacun de ses pas jaillissait le soulagement de la terre, en herbe neuve et ruisseau frais, où pointait un timide museau. La chaleur de leurs retrouvailles amollit l’atmosphère. La nature déversait ses bienfaits. Les femmes s’y délassaient. Adorant Dionysos, nous portions la peau de daim et le sceptre de pin, nous mangions les fruits à peine cueillis et la chair crue à même la carcasse. Nous étions aussi libres que les bêtes et les dieux, règnes renversés dans le renouveau. La terre que nous foulions s’échauffait, toutes les formes d’amour s’y célébraient.

Telle est la loi de la forêt, la loi de la vie. Orphée la transgressait. Il aurait voulu que le monde s’effondrât au dernier souffle d’Eurydice, et le monde ne cédait pas d’un pouce, mais lui faisait comme si.

Il dérangeait.

Ce n’était pas que l’on enviait Eurydice, nous, les femmes, ni l’amour dont elle était l’objet, c’était que l'on déteste facilement celui qui dans la passion nous annonce la souffrance et dans la vie nous prévient de la mort, celui aussi qui, plus fidèle, nous rappelle que l’on oublie. La pureté d’Orphée insultait à la contagion dévorante de la vie par la vie. Ombre de lui-même, il allait sur terre comme il l’aurait fait en enfer. Il ne changeait pas. Il était mort déjà.

Nous étions grisées par une ivresse sans substance, de pollen, de tiédeur, de tendresse, étourdies d’avoir survécu une fois de plus à l’hiver, ce qui dans nos montagnes n’est pas donné au tout venant, envahies d’une joie qui n’appartient ni à toi ni à moi, mais à notre corps à corps, à la vie qui déborde, se répand et gagne du terrain, le terrain d’un printemps, sur l’éternelle mort. Nous étions amoureuses, d’Orphée comme de tous les hommes et les unes des autres, mais lui n’avait plus aucun amour à donner.

Dionysos perdait patience. Il estimait Orphée, ce qu’il avait d’absolu, d’insensé, sa familiarité avec les fauves, son intimité avec les éléments, sa révérence envers la vie jusqu’aux confins de la mort. Mais il ne permettait pas à qui que ce soit de se soustraire au printemps. C’était lui qui organisait les festivités et nous y obligeait, déchaînant les passions à chaque saison. Il s’agissait, disait-il, d’une question de santé publique. "Le délire délie votre raison lente et laborieuse, les excès garantissent votre tempérance à venir, le dérèglement remet à zéro votre cœur comptable. Ainsi communient les êtres qui ne savent plus communiquer, et les humains se réconcilient avec la nature vierge et sauvage dont ils font partie."

Sur son ordre, celui qui fuit la fête, la fête le poursuit. Orphée fut rattrapé. Nous sommes allées le chercher dans sa grotte. Il ne nous a même pas regardées. Il fixait le mur définitif entre lui et Eurydice. Parfois, des mains, il l’auscultait et imaginait sentir une chaleur répondre à la sienne. Nous ne parlions pas plus que lui. Nous chantions sans langage, une sorte de râle, accompagné de tambours et cymbales.

Nous l’avons soulevé, nous l’avons emporté, nous avons dansé en ronde autour de lui. Il ne nous reconnaissait pas. Des dessins défiguraient nos traits. Il ne tenait plus debout après l’hiver. Comment, en trois ans de vie sauvage, Orphée n’était-il pas mort de froid ou de faim ? Hadès, pour se venger de l’outrage, retenait-il sa descente aux enfers ?

"Chante, Orphée.\
  Allez, chante.\
      Décris la grâce des ménades.\
            Dis-nous notre beauté."

On le bousculait, on le redressait, il retombait. L’une d’entre nous entra dans le cercle. Elle se pencha sur lui, prit son visage et embrassa cette bouche tue, la cicatrice ardente laissée par Eurydice. Il la repoussa. Elle le fouetta de son thyrse. Une autre la rejoignit et le griffa de ses ongles. La danse se mua en chasse.

On était massées sur lui, à le tirer chacune de notre côté. Une de ces curées de ménades qui finissent par la mise en pièces d’une bête encore pantelante. Les os furent déboîtés, rompus, la chair arrachée. Il se taisait. Les seuls hurlements étaient les nôtres.

Nous avons déchiré Orphée. Nous, femmes de Thrace, ses compatriotes, filles de Rhodope. J’avais un bras de lui et je ne sais pas si je l’aurais mangé. Je ne sais pas. Sa tête a roulé à mes pieds. Elle ne dit qu’un mot : "Eurydice".

La voix nous dégrisa. J’y ai souvent pensé depuis et j’ai tant essayé d’imaginer ce qu’il a ressenti que j’ai oublié ce que j’ai ressenti. Comme il doit être terrible de mourir en connaissant la mort, en sachant en toute certitude et conscience qu’il n’y a plus pour soi que la poussière.

Nous sommes restées figées dans le sang répandu. Dionysos nous a punies à la hauteur du crime. Il a changé en vignes les ménades. Sur un coteau, dans la vallée, elles donnent le vin et transmettent leur fureur à qui le boit, condamnées à saigner sans cesse sous la presse pour le sang qu’elles ont versé. J’ai été épargnée afin de garder la grotte d’Orphée, devenue un temple pour les égarés. Il n’y a pas de meilleur dévouement que la culpabilité.

La source, mère d’Eurydice, recueillit la tête, elle la porta à la rivière, qui la porta au fleuve. Elle a traversé ainsi de nombreux pays, avant d’échouer sur une rive. Les gens du lieu la traitent en oracle de l’au-delà, en prophète d’une religion nouvelle.

Je ne sais plus, Orphée, si la vie que tu aimais éperdument, au point de ne jamais parvenir à la haïr, si la vie est si belle. Elle s’est montrée sous mes yeux atroce et cruelle. Tu devrais le savoir. Eurydice violentée par Aristée, et toi écartelé par les ménades, tous deux tués pour avoir résisté au désir imposé, et après toutes ces épreuves pour la réincarner, tu finis par être littéralement désincarné. Quel est le sens de vos vies sacrifiées ?

Je ne sais plus si Dionysos nous guidait, nous égarait, ou s'était absenté. Il ne voulait pas te réduire à sa cause par une telle torture, mais vous partagiez les mêmes lieux, et tout en étant proches, vous étiez opposés. La viande qu’il nous invitait à manger crue, tu n’y touchais même pas quand elle était cuite. En souvenir de ta bien-aimée, tu dédaignais la chasse qu’il réintroduisit dans notre forêt. Tu pratiquais la chasteté, quand il professait la débauche. Vous déliriez chacun à votre façon, et Rhodope n’avait pas de place pour deux fous aussi différents.

Seul Apollon aurait pu te protéger de lui. Longtemps, il t’avait traité comme un frère, partageant avec toi ses envolées à dos de cygne et sa pêche aux comètes à crête de montagne, mais il t’avait abandonné après ton retour des enfers. Il désirait une poésie solaire, celle d’un esprit clair et distinct et d’une âme saine dans un corps sain, pas une poésie des cendres, qui descend déterrer les morts et jouer aux osselets avec son ombre. Il se détourna, dégoûté, du bégaiement de ton esprit face au néant, de la vérité sans réplique de ta bouche tailladée par le deuil.

Je te fais une promesse, Orphée. Je continuerai à regarder la paroi, à la toucher, à l’écouter. Si Eurydice revient, elle n’aura pas faim, elle n’aura pas froid. Une amie l’attend.

Et vous, mes juges, voyez, je ne pleure pas, je ne demande aucune pitié, mais soyez justes. Mesurez votre liberté dans la foule, votre lucidité dans la passion, votre bonté entourée par le mal, et décrétez à cette aune ma culpabilité. Les ménades expriment une nécessité naturelle, aussi innocente et implacable que la foudre. J’en suis la dernière étincelle.

# 🜔

Orphée fut notre prophète.

Sa tête échoua sur nos rives, entre les roseaux, nous lavions le poisson. Le courant battait contre ses dents et animait sa langue. Il nous décrivit l’origine du monde.

Au début était un œuf, ou une graine. L’univers est issu de son éclosion. Il est la division, la dispersion de cet être premier, la croissance écartelée de l’unité enclose.

Dans ce monde créé, Perséphone eut un fils. Avant d’être privée du soleil, de la course libre, de son village candide, et de vivre stérile dans un palais d’os, enserré d’un jardin de venins, la jeune Coré donna naissance à Zagreus, fils d’aucun père, et donc du père de chacun : Zeus. Les Titans lui dérobèrent l’enfant, jouèrent à le lancer entre eux et finirent, ennuyés, par le dévorer. Zeus les foudroya aussitôt. Coré, les cheveux défaits, aveuglée de larmes, mangea ce qui restait de son fils : le cœur. Ainsi, elle lui redonna naissance. Zagreus ressuscita en Dionysos. Des cendres mêlées des meurtriers et de la victime s’exhalait une fumée qui prit forme et consistance : les humains apparurent sur terre. Ils tenaient leur rage des Titans et leur lumière de l’Enfant.

Un jour, comme Dionysos, l’univers sera réunifié. Mais chacun peut déjà toucher à la fusion première, au cœur de l’univers, en renonçant à la part du Titan et en encourageant celle de l’Enfant.

Quelques-uns parmi nous protestèrent aux paroles d’Orphée. Nos maîtres nous avaient enseigné une tout autre histoire. Selon eux, l’unité de l’origine n’était qu’un terrible chaos, qui s’organisa en se différenciant en terre, ciel et mer, puis en dieux, hommes et animaux, et nos cités participaient à l’ordonnance du monde, chacun ayant sa place et sa tâche. Nos arts et nos techniques donnaient forme à l’informe et le langage nous permettaient de distinguer chaque chose. Les temps anciens se perdaient dans une barbarie universelle, nous avancions vers la civilisation.

Orphée nous conseilla de renier de si mauvais maîtres. Lui savait. Il était déjà allé dans les enfers et bientôt y retournerait.

Il nous raconta que les êtres se réincarnaient infiniment et que leur vie précaire et douloureuse n’était qu’une sorte de mort, mais qu’un chemin permettait d’échapper à ces tourments et de réintégrer la divinité dont une parcelle nous anime.

En descendant en enfer, la première souffrance est la soif. Deux eaux irriguent la poussière, on entend leur bruissement dans les ténèbres. Léthé et Mnémosyne, Oubli et Mémoire. La première, une rivière bordée de cyprès blancs, coule au vu et au su de tous, mais la deuxième, lac où s’abreuve un taureau écarlate, se dissimule au milieu d’un labyrinthe dont seule Perséphone connaît le dessin. Si elle reconnaît en toi son fils, Zagreus, elle te montrera comment trouver Mémoire, tu te rappelleras ton origine céleste et la rejoindras. Mais si en ton sein le Titan l’emporte sur l’Enfant, dévorant jusqu’à l’ultime lumière, elle t’abandonnera sur les rives de l’Oubli et tu devras recommencer l’épreuve de la vie.

Orphée nous donna des règles pour purifier notre âme : se déprendre de la satisfaction et de la complaisance, se préserver de la violence, ne jamais verser le sang ni profiter de la faiblesse, et donc ne pas manger de chair ni participer à la guerre, ne pas porter de laine ni de cuir, se vêtir de blanc, quitter nos cités, vivre dans la forêt, au moins pour un temps, y chercher la lumière divine qui se manifeste dès cette vie, partout et très communément, ne pas exclure les femmes qui, comme Perséphone, savent nous guider, apprendre à aimer, mieux que lui-même n’a aimé Eurydice, puisqu’il l’a perdue, être fidèle, se souvenir de nos rêves au réveil, ils en savent plus long que nos raisonnements.

Autour de sa tête tranchée, à force de discours presque réduite au crâne, la nature se réunissait. Le courant retenait ses flots, les palmiers se penchaient. Le lion se réconciliait avec la gazelle, l’oiseau avec l’archer, même Déméter et Hadès, Apollon et Dionysos s’entendaient à merveille ; et tous encerclaient le poète. Ce miracle nous persuada de la vérité de ses oracles. Un jour reviendra cette harmonie ancienne, nouvelle que sa parole annonçait.

# 🜃

Eurydice. ---







            Orphée !
